package nl.ensignprojects.test.oauth2.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserController {

    @GetMapping("/api/v1/user")
    public ResponseEntity<?> getUser(OAuth2AuthenticationToken token) {
        log.info("token: {}", token);

        OidcUser user = (OidcUser) token.getPrincipal();

        return ResponseEntity.ok(user.getGivenName());
    }
}
