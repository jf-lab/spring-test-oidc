package nl.ensignprojects.test.oauth2.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello-world")
public class HelloWorldController {

    @GetMapping(path = "/unsafe")
    public ResponseEntity<?> helloWorldWithoutSecurity() {
        return ResponseEntity.ok("Hello, unsafe world!");
    }

    @GetMapping(path = "/secure")
    public ResponseEntity<?> helloWorldWithSecurity() {
        return ResponseEntity.ok("Hello, secure world!");
    }

    @PreAuthorize("#id == authentication.principal.attributes.get('tenant')")
    @GetMapping(path = "/tenant/{id}" )
    public ResponseEntity<?> resolveTenantId(@PathVariable String id ) {
        return ResponseEntity.ok("This is the id: " + id);
    }
}
