package nl.ensignprojects.test.oauth2.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.StandardClaimNames;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;

import static java.util.Collections.singletonList;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SecurityTests {

    @Autowired
    private WebApplicationContext webapp;

    private MockMvc mock;

    @BeforeEach
    void setUp() {
        mock = MockMvcBuilders.webAppContextSetup(webapp)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Without authentication")
    void testUnsafe() throws Exception {
        ResultActions resultActions = mock.perform(get("/hello-world/unsafe")).andDo(print());
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(content().string("Hello, unsafe world!"));
    }

    @Test
    @DisplayName("Authenticated endpoint, when it fails")
    void testSecure() throws Exception {
        ResultActions resultActions = mock.perform(get("/hello-world/secure")).andDo(print());
        resultActions.andExpect(status().isFound());
    }

    @Test
    @WithMockUser(username="bla", roles = {"USER", "ADMIN"})
    @DisplayName("Authenticated endpoint, when it is successful")
    void testSecureSuccess() throws Exception {
        ResultActions resultActions = mock.perform(get("/hello-world/secure")).andDo(print());
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(content().string("Hello, secure world!"));

    }

    @Test
    @DisplayName("Endpoint which needs an Oauth2Token")
    void testWithToken() throws Exception {
        GrantedAuthority ga = () -> "ROLE_USER";

        HashMap<String, Object> claims = new HashMap<>();
        claims.put(StandardClaimNames.SUB, "test@email.nl");
        claims.put(StandardClaimNames.GIVEN_NAME, "Test Persoon");

        OidcIdToken oidcToken = new OidcIdToken("bla", Instant.now(), Instant.now().plusSeconds(13), claims);
        DefaultOidcUser defaultOidcUser = new DefaultOidcUser(new HashSet<>(singletonList(ga)), oidcToken);
        OAuth2AuthenticationToken oAuth2AuthenticationToken = new OAuth2AuthenticationToken(defaultOidcUser, null, "okta");

        ResultActions resultActions = mock.perform(get("/api/v1/user").with(authentication(oAuth2AuthenticationToken))).andDo(print());
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(content().string("Test Persoon"));
    }

    @Test
    @DisplayName("Tenant allow")
    void testTenantAllow() throws Exception {
        GrantedAuthority ga = () -> "ROLE_USER";

        HashMap<String, Object> claims = new HashMap<>();
        claims.put(StandardClaimNames.SUB, "test@email.nl");
        claims.put(StandardClaimNames.GIVEN_NAME, "Test Persoon");
        claims.put("tenant", "test");

        OidcIdToken oidcToken = new OidcIdToken("bla", Instant.now(), Instant.now().plusSeconds(13), claims);
        DefaultOidcUser defaultOidcUser = new DefaultOidcUser(new HashSet<>(singletonList(ga)), oidcToken);
        OAuth2AuthenticationToken oAuth2AuthenticationToken = new OAuth2AuthenticationToken(defaultOidcUser, null, "okta");

        ResultActions resultActions = mock.perform(get("/hello-world/tenant/test").with(authentication(oAuth2AuthenticationToken))).andDo(print());
        resultActions.andExpect(status().isOk());
    }

    @Test
    @DisplayName("Tenant deny")
    void testTenantDeny() throws Exception {
        GrantedAuthority ga = () -> "ROLE_USER";

        HashMap<String, Object> claims = new HashMap<>();
        claims.put(StandardClaimNames.SUB, "test@email.nl");
        claims.put(StandardClaimNames.GIVEN_NAME, "Test Persoon");
        claims.put("tenant", "test");

        OidcIdToken oidcToken = new OidcIdToken("bla", Instant.now(), Instant.now().plusSeconds(13), claims);
        DefaultOidcUser defaultOidcUser = new DefaultOidcUser(new HashSet<>(singletonList(ga)), oidcToken);
        OAuth2AuthenticationToken oAuth2AuthenticationToken = new OAuth2AuthenticationToken(defaultOidcUser, null, "okta");

        ResultActions resultActions = mock.perform(get("/hello-world/tenant/test123").with(authentication(oAuth2AuthenticationToken))).andDo(print());
        resultActions.andExpect(status().isForbidden());
    }
}
